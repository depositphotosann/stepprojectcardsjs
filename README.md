# Step Project Cards

Before log in you have to register [here] (https://ajax.test-danit.com/front-pages/cards-register.html).Enter your email and password(at least 8 characters)
After that you will be able to log in on our website.


## _Team_
### _Anna Kiryak_
- Serch/filter functions
- Render "Create visit" modal window
- Create class Visit, methods and design for the "Card that describes the visit"
- Filter form design
- Basic general design

### _Alisa Bielenko_
- Functions for working with API
- Local storage logic
- Basic general design
- Classes and methods for "Log in" modal window
